﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    class MirrorComparator
    {

        public int Compare(Mirror x, Mirror y)
        {
            double a = GeneticMath.FitnessFunction(x) - GeneticMath.FitnessFunction(y);
            if (a > 0) return -1;
            else if (a < 0) return 1;
            else return 0;
        }
    }
}
