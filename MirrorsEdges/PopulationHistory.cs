﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    internal class PopulationHistory
    {
        private static volatile PopulationHistory instance;
        private static object syncRoot = new Object();
        private List<Population> populations;

        private PopulationHistory()
        {
            populations = new List<Population>();
        }

        public static PopulationHistory Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new PopulationHistory();
                    }
                }

                return instance;
            }
        }

        public void addPopulation(Population population)
        {
            populations.Add(population);   
        }

        public ResultMirror[] getResultMirrors()
        {
            ResultMirror[] tab = new ResultMirror[populations.Count];

            for (int i = 0; i < populations.Count; i++)
            {
                Mirror best = populations[i].BestMirror;
                tab[i] = new ResultMirror(best, best.FitnessFunctionValue);
            }
     
            return tab;
        }

        public void Clear()
        {
            populations.Clear();
        }
    }
}
