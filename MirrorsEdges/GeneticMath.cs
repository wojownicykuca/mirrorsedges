﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Single;


namespace MirrorsEdges
{
    class GeneticMath
    {
        private static Random rand = new Random();

        public static float GetNormalDistributedRandom(float mean, float standardDeviation)
        {
            float u1 = (float)rand.NextDouble(); //these are uniform(0,1) random doubles
            float u2 = (float)rand.NextDouble();
            float randStdNormal = (float)(Math.Sqrt(-2.0 * Math.Log(u1)) *
                         Math.Sin(2.0 * Math.PI * u2)); //random normal(0,1)
            float randNormal =
                         mean + standardDeviation * randStdNormal; //random normal(mean,stdDev^2)
            return randNormal;
        }

        public static float SegmentFittnes(Mirror mirror, int segment)
        {
            float[] rayArray = { 0.0f, -1.0f };

            Vector ray = new DenseVector(rayArray);

            Vector normal = mirror.GetSegment(segment).GetNormalVector();

            normal = (Vector)normal.Normalize(2.0);

            Vector reflection = ray;

            reflection = (Vector)reflection.Subtract(2 * (normal * ray) * normal);

            reflection = (Vector)reflection.Normalize(2);

            Vector focus = mirror.GetSegment(segment).GetFocusPointVector();

            focus = (Vector)focus.Normalize(2);

            float add = (float)(180 * Math.Acos(focus * reflection) / Math.PI);
            if (add > 1) add *= add;

            return add;
        }

        public static float FitnessFunction(Mirror mirror)
        {
            float[] rayArray = { 0.0f, -1.0f };
            float fitnessFunctionValue = 0.0f;

            Vector ray = new DenseVector(rayArray);

            for (int i = 0; i < mirror.GetSegmentCount(); i++)
            {
                fitnessFunctionValue += SegmentFittnes(mirror, i);
            }




            return fitnessFunctionValue/mirror.GetSegmentCount();
        }


    }
}
