﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using MathNet.Numerics.LinearAlgebra.Single;

namespace MirrorsEdges
{
    /// <summary>
    /// Class representing a single mirror segment.
    /// </summary>
    class Segment
    {
        /// <summary>
        /// First (left) mirror point.
        /// </summary>
        private PointF firstPoint;
        
        /// <summary>
        /// Second (right) mirror point.
        /// </summary>
        private PointF secondPoint;
  

        /// <summary>
        /// Makes a single mirror segment.
        /// </summary>
        /// <param name="first">Left point.</param>
        /// <param name="second">Right point.</param>
        public Segment(float firstX, float firstY, float secondX, float secondY)
        {
            firstPoint = new PointF();
            secondPoint = new PointF();
            if (/*firstY <= 0 && secondY <= 0 &&*/ firstX >= -50.0 && firstX <= 50.0 && secondX >= -50.0 && secondX <= 50.0)
            {
                if (firstX < secondX)
                {
                    firstPoint.X = firstX;
                    secondPoint.X = secondX;
                    firstPoint.Y = firstY;
                    secondPoint.Y = secondY;
                }
                else
                {
                    firstPoint.X = secondX;
                    secondPoint.X = firstX;
                    firstPoint.Y = secondY;
                    secondPoint.Y = firstY;
                }
            }
            else throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// Returns left mirror point.
        /// </summary>
        public PointF First
        {
            get
            {
                return firstPoint;
            }
        }

        /// <summary>
        /// Returns right mirror point.
        /// </summary>
        public PointF Second
        {
            get
            {
                return secondPoint;
            }
        }

        public DenseVector GetNormalVector()
        {
            float[] v = { -(secondPoint.Y - firstPoint.Y), secondPoint.X - firstPoint.X };
            DenseVector vec = new DenseVector(v);

            return vec;
        }

        public DenseVector GetFocusPointVector()
        {
            float[] v = { GlobalSettings.Instance.FocusPointPosition.X - (secondPoint.X + firstPoint.X) / 2, GlobalSettings.Instance.FocusPointPosition.Y - (secondPoint.Y + firstPoint.Y) / 2 };       
            DenseVector vec = new DenseVector(v);

            return vec;
        }
    }
}
