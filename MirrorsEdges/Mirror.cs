﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
 
namespace MirrorsEdges
{
    /// <summary>
    /// Class for handling mirrors.
    /// </summary>
    class Mirror : ICloneable
    {
        /// <summary>
        /// Number of points in mirror
        /// </summary>
        private List<float> points;
        /// <summary>
        /// Indicates how far from 0 can mirror point get while it's generated
        /// </summary>
        private const float pointSpan = 50;
        /// <summary>
        /// Max point's distance from origin.
        /// </summary>
        public const float MaxPointValue = 50;
        /// <summary>
        /// Min point's distance from origin.
        /// </summary>
        public const float MinPointValue = 0;

        private bool changed = true;

        private float fitnessFunctionValue;

        /// <summary>
        /// Default constructor. Makes mirror with no points
        /// </summary>
        public Mirror()
        {
            points = new List<float>();
        }

        /// <summary>
        /// Makes random mirror using random seed (time based)
        /// </summary>
        /// <param name="pointCount">How much poinst mirror should have</param>
        public Mirror(int pointCount)
        {
            points = new List<float>();

            for (int i = 0; i < pointCount; i++)
            {
                if (i == 0 || i == pointCount-1) points.Add(0.0f);
                else points.Add((float)-GlobalSettings.Instance.rnd.NextDouble() * pointSpan);
            }
        }

        /// <summary>
        /// Makes random mirror usin specifed seed
        /// </summary>
        /// <param name="pointCount">How much poinst mirror should have</param>
        /// <param name="seed">Seed for random generation</param>
        public Mirror(int pointCount, int seed)
        {
            Random r = new Random(seed);

            points = new List<float>(pointCount);

            for (int i = 0; i < pointCount; i++)
            {
                if (i == 0 || i == pointCount - 1) points[i]= 0.0f;
                points[i] = (float)(-r.NextDouble() * pointSpan);
            }
        }

        /// <summary>
        /// Returns list of points inside a mirror.
        /// </summary>
        public float GetPoint(int index)
        {
            return points[index];
        }

        public void SetPoint(int index, float value)
        {
            if (index == 0 || index == points.Count - 1) return;
            points[index] = value;
            changed = true;
        }

        /// <summary>
        /// Returns a single segment of a mirror.
        /// </summary>
        /// <param name="segmentIndex">Index of segment (from 0 to pointCount-1)</param>
        /// <returns>Single mirror segment.</returns>
        public Segment GetSegment(int segmentIndex)
        {
            if (segmentIndex < points.Count - 1)
                return new Segment(
                                    (100.0f / (points.Count-1)) * segmentIndex - 50.0f, 
                                    points[segmentIndex],
                                    ((100.0f / (points.Count-1)) * (segmentIndex+1)) - 50.0f, 
                                    points[segmentIndex + 1]
                                  );
            else throw new ArgumentOutOfRangeException();
        }

        public int GetSegmentCount()
        {
            return points.Count - 1;
        }
        
        public int GetPointCount()
        {
            return points.Count;
        }

        public void AddPoint(float point)
        {
            points.Add(point);
            changed = true;
        }

        public PointF GetMirrorVertex(int i)
        {
            float pCount = points.Count - 1;
            return new PointF(((i-pCount/2.0f)/(pCount/2.0f)) * 50.0f, points[i]);
        }

        public float FitnessFunctionValue
        {
            get
            {
                if (changed)
                {
                    fitnessFunctionValue = GeneticMath.FitnessFunction(this);
                    changed = false;
                }
                return fitnessFunctionValue;
            }
        }

        public object Clone()
        {
            Mirror clone = new Mirror();
            foreach (float point in points)
            {
                clone.AddPoint(point);
            }
            clone.fitnessFunctionValue = this.FitnessFunctionValue;
            clone.changed = false;

            return clone;
        }
    }
}
