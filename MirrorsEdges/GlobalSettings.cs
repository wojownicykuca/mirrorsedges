﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    public class GlobalSettings
    {
        private static volatile GlobalSettings instance;
        private static object syncRoot = new Object();

        /// <summary>
        /// tryb wykonania algorytmu
        /// </summary>
        public mode CalculationMode { get; set; }
        /// <summary>
        /// do jakiej wartosci wspaolczynnika ma sie wykonywac algorytm
        /// </summary>
        public double ReflactanceLimit { get; set; }
        /// <summary>
        /// do ktorej populacji algorytm ma sie wykonywac
        /// </summary>
        public int GenerationLimit { get; set; }
        /// <summary>
        /// lustrer w populacji
        /// </summary>
        public int MirrorsInPopulation =50;
        /// <summary>
        /// punktow w lustrze
        /// </summary>
        public int PopulationCount =20;

        public Random rnd = new Random();

        private GlobalSettings() 
        {
            FocusPointPosition = new PointF(0.0f, 0.0f); // dlaczego to jest doublem, a nie punktem?
            CalculationMode = mode.UP_TO_GENERATION;
            GenerationLimit = 10000;
            ReflactanceLimit = 0.1;
            MirrorsInPopulation = 50;
            PopulationCount = 20;
        }

        public static GlobalSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new GlobalSettings();
                    }
                }

                return instance;
            }
        }

        public PointF FocusPointPosition
        {
            get;
            set;
        }

    }
    /// <summary>
    /// działanie przez dana ilość pokoleneń
    /// lub do uzyskania zadowalającego wspołczynnika
    /// </summary>
    public enum mode { UP_TO_REFLACTANCE, UP_TO_GENERATION };
}
