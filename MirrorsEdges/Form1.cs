﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MirrorsEdges
{
    public partial class Form1 : Form
    {
        private PopulationHistory populationHistory;
        private ResultMirror[] result;
        private int currentMirrorIndex;
        private Graphics g;

        public Form1()
        {
            InitializeComponent();
            g = this.panel2.CreateGraphics();
            this.PreparePopulations(new Population());
        }

        private void PreparePopulations(Population population)
        {
            PopulationHistory.Instance.Clear();
            PopulationHistory.Instance.addPopulation(population);
            this.result = PopulationHistory.Instance.getResultMirrors();
            this.currentMirrorIndex = 0;
        }
        
        private void DrawMirrorVertex(PointF p)
        {
            this.DrawCircleAt(p, 5, Color.Black);
        }

        private void DrawFocusPoint()
        {
            this.DrawCircleAt(GlobalSettings.Instance.FocusPointPosition, 7, Color.Red);
        }

        private void DrawCircleAt(PointF pp, float radius, Color color)
        {
            SolidBrush br = new SolidBrush(color);
            PointF p = setScale(pp);
            g.FillEllipse(br, p.X - radius, p.Y - radius, 2 * radius, 2 * radius);
        }

        private void DrawLine(PointF pp1, PointF pp2)
        {
            PointF p1 = setScale(pp1);
            PointF p2 = setScale(pp2);
            g.DrawLine(Pens.Black, p1, p2);
        }

        private void DrawMirror()
        {
            ResultMirror m = this.result[this.currentMirrorIndex];
            int size = m.Mirror.GetPointCount()-1;
            float width = (float)this.FocusXBox.Maximum;
            for (int i = 0; i < size; ++i)
            {
                this.DrawLine(m.Mirror.GetMirrorVertex(i), m.Mirror.GetMirrorVertex(i+1));
            }
            for (int i = 0; i <= size; ++i)
                this.DrawMirrorVertex(m.Mirror.GetMirrorVertex(i));
        }

        private PointF setScale(PointF pp)
        {
            float margin = 1;
            float scaleX = (float)this.panel2.Width - 2 * margin;
            float scaleY = ((float)this.panel2.Height - 2 * margin);
            PointF p = pp;

            p.X = ((pp.X+50) / 100.0f) * scaleX;
            p.Y = ((pp.Y + Mirror.MaxPointValue) / 150.0f) * scaleY;
            
            p.Y = (float)this.panel2.Height - p.Y;
            return p;
        }

        private void DrawPanel()
        {
            this.DrawFocusPoint();
            this.DrawMirror();
            this.GenerationNumLabel.Text = "Numer pokolenia: " + this.currentMirrorIndex.ToString();
            this.ReflectanceLabel.Text = "Współczynnik zwieciadła:\n\t" + this.result[this.currentMirrorIndex].FittnessFunctionValue;
        }

        #region zdarzenia

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            if (this.result.Length > 0)
            {
                this.DrawPanel();
            }
        }
        
        private void UpToReflectance_CheckedChanged(object sender, EventArgs e)
        {
            if (this.UpToReflectanceRadioBox.Checked == true)
            {
                this.UpToReflectanceValueBox.Enabled = true;
                GlobalSettings.Instance.CalculationMode = mode.UP_TO_REFLACTANCE;

            }
            else this.UpToReflectanceValueBox.Enabled = false;
        }

        private void UpToGenerationConditionRadioBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.UpToGenerationConditionRadioBox.Checked == true)
            {
                this.UpToGenerationConditionValueBox.Enabled = true;
                GlobalSettings.Instance.CalculationMode = mode.UP_TO_GENERATION;
            }
            else this.UpToGenerationConditionValueBox.Enabled = false;
        }

        private void FocusPointX_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.Instance.FocusPointPosition = new PointF((float)FocusXBox.Value, (float)FocusYBox.Value);
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void FocusPointY_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.Instance.FocusPointPosition = new PointF((float)FocusXBox.Value, (float)FocusYBox.Value);
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void UpToReflectance_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.Instance.ReflactanceLimit = (double)this.UpToReflectanceValueBox.Value;
        }

        private void UpToGeneration_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.Instance.GenerationLimit = (int)this.UpToGenerationConditionValueBox.Value;
        }

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            Population first = new Population((int)this.PointsNumberBox.Value, (int)this.MirrorsNumber.Value);
            this.PreparePopulations(first);
            Population next = first;
            this.currentMirrorIndex = 0;

            GlobalSettings.Instance.GenerationLimit = 1000;

            //for (int i = 0; i < GlobalSettings.Instance.GenerationLimit; i++)
            while (next.BestMirror.FitnessFunctionValue > 800)
            {
                Population pop = GeneticAlgorithm.GenerateNextPopulation(next, GlobalSettings.Instance.FocusPointPosition.Y);
                PopulationHistory.Instance.addPopulation((Population)pop.Clone());
                next = pop;
                
                this.GenerationNumLabel.Text = "Numer pokolenia: " + this.currentMirrorIndex.ToString();
                this.ReflectanceLabel.Text = "Współczynnik zwieciadła:\n\t" + next.BestMirror.FitnessFunctionValue.ToString();
                this.GenerationNumLabel.Refresh();
                this.ReflectanceLabel.Refresh();
                this.currentMirrorIndex++;
            }


            this.panel2.Invalidate();
            this.result = PopulationHistory.Instance.getResultMirrors();
            this.DrawPanel();

            
            //TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

            
        }

        private void PointsNumber_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.Instance.MirrorsInPopulation = (int)this.PointsNumberBox.Value;
        }

        private void MirrorsNumber_ValueChange(object sender, EventArgs e)
        {
            GlobalSettings.Instance.PopulationCount = (int)this.MirrorsNumber.Value;
        }

        private void Prev_Click(object sender, EventArgs e)
        {
            if (this.result.Length <= 0)
                return;
            this.currentMirrorIndex--;
            if (this.currentMirrorIndex < 0)
                this.currentMirrorIndex = 0;
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void PrevSeveral_Click(object sender, EventArgs e)
        {
            if (this.result.Length <= 0)
                return;
            this.currentMirrorIndex-=5;
            if (this.currentMirrorIndex < 0)
                this.currentMirrorIndex = 0;
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (this.result.Length <= 0)
                return;
            this.currentMirrorIndex++;
            if (this.currentMirrorIndex > this.result.Length-1)
                this.currentMirrorIndex = this.result.Length - 1;
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void NextSeveral_Click(object sender, EventArgs e)
        {
            if (this.result.Length <= 0)
                return;
            this.currentMirrorIndex+=5;
            if (this.currentMirrorIndex > this.result.Length - 1)
                this.currentMirrorIndex = this.result.Length - 1;
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void Best_Click(object sender, EventArgs e)
        {
            if (this.result.Length <= 0)
                return;
            this.findBest();
            this.panel2.Invalidate();
            this.DrawPanel();
        }

        private void findBest()
        {
            //indeks najlepszego lustra
            this.currentMirrorIndex = 0;
        }

        #endregion
    }
}
