﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    /// <summary>
    /// Class for mirrors population
    /// </summary>
    class Population : ICloneable
    {
        /// <summary>
        /// List of mirrors inside population
        /// </summary>
        private List<Mirror> mirrors;

        /// <summary>
        /// Default constructor. Makes empty population.
        /// </summary>
        public Population()
        {
            mirrors = new List<Mirror>();
            for (int i = 0; i < GlobalSettings.Instance.MirrorsInPopulation; i++)
            {
                mirrors.Add(new Mirror(GlobalSettings.Instance.PopulationCount));
            }
        }

        /// <summary>
        /// Makes random population with random seed.
        /// </summary>
        /// <param name="populationCount">How many mirrors should be in population.</param>
        /// <param name="mirrorPointCount">How many points a mirror should consist.</param>
        public Population(int populationCount, int mirrorPointCount)
        {
            mirrors = new List<Mirror>();

            for (int i = 0; i < populationCount; i++)
            {
                mirrors.Add(new Mirror(mirrorPointCount));
            }
        }

        /// <summary>
        /// Makes random population with specified seed
        /// </summary>
        /// <param name="populationCount">How many mirrors should be in population.</param>
        /// <param name="mirrorPointCount">How many points a mirror should consist.</param>
        /// <param name="seed">Seed for random number generator.</param>
        public Population(int populationCount, int mirrorPointCount, int seed)
        {
            mirrors = new List<Mirror>();

            for (int i = 0; i < populationCount; i++)
            {
                mirrors.Add(new Mirror(mirrorPointCount, seed));
            }
        }

        /// <summary>
        /// Returns list of mirrors inside the population
        /// </summary>
        public List<Mirror> Mirrors
        {
            get
            {
                return mirrors;
            }
        }

        public Mirror BestMirror
        {
            get
            {
                double best = double.MaxValue;
                double cur = 0;
                Mirror bestMirror = mirrors[0];
                for (int i = 0; i < mirrors.Count; i++)
                {
                    cur = mirrors[i].FitnessFunctionValue;
                    if (cur < best)
                    {
                        best = cur;
                        bestMirror = mirrors[i];
                    }
                }

                return bestMirror;
            }

        }

        public object Clone()
        {
            Population clone = new Population();

            foreach (Mirror mirror in Mirrors)
            {
                clone.mirrors.Add((Mirror)mirror.Clone());
            }

            return clone;
        }
    }
}
