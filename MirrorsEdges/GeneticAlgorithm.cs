﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    /// <summary>
    /// Class for genetic algorithm.
    /// </summary>
    class GeneticAlgorithm
    {
        /// <summary>
        /// How strong a mutation should be.
        /// </summary>
        private const float mutationStrength = 10.0f;
        /// <summary>
        /// Number of chidren made on each algorithm iteration
        /// </summary>
        private const int offspringCount = 100;
        /// <summary>
        /// How many mirrors will be mutated
        /// </summary>
        private const int mutantCount = 50;


        /// <summary>
        /// Generates new population using current one.
        /// </summary>
        /// <param name="currentPopulation"></param>
        /// <returns>A new population.</returns>
        public static Population GenerateNextPopulation(Population currentPopulation, double focusPoint)
        {
            Random r = new Random();

            Population population = currentPopulation;

            MakeChildren(ref population);
            MakeMutants(ref population);
            KillWeakest(ref population, focusPoint);            

            return population;
        }

        private static int CompareMirrors(Mirror a, Mirror b)
        {
            double result = b.FitnessFunctionValue - a.FitnessFunctionValue;
            if (result > 0) return -1;
            else if (result < 0) return 1;
            return 0;
        }


        private static void MakeChildren(ref Population population)
        {
            Random r = new Random();
            Comparison<Mirror> compare = CompareMirrors;

            //population.Mirrors.Sort(compare);

            for (int i = 0; i < offspringCount; i++)
            {
                int firstParentIndex, secondParentIndex;
                //firstParentIndex = (int)GeneticMath.GetNormalDistributedRandom(0.0f, population.Mirrors.Count / 3.0f);
                //secondParentIndex = (int)GeneticMath.GetNormalDistributedRandom(0.0f, population.Mirrors.Count / 3.0f);

                //firstParentIndex = Math.Abs(firstParentIndex);
                //secondParentIndex = Math.Abs(secondParentIndex);

                //if (firstParentIndex >= population.Mirrors.Count) firstParentIndex = population.Mirrors.Count - 1;
                //if (secondParentIndex >= population.Mirrors.Count) secondParentIndex = population.Mirrors.Count - 1;

                firstParentIndex = r.Next(0, population.Mirrors.Count - 1);
                secondParentIndex = r.Next(0, population.Mirrors.Count - 1);
                
                if (firstParentIndex == secondParentIndex)
                {
                    secondParentIndex = (secondParentIndex + 1) % population.Mirrors.Count; // we don't cross mirror with itself
                }

                Mirror child = Crossover(population.Mirrors[firstParentIndex], population.Mirrors[secondParentIndex]);

                population.Mirrors.Add(child);
            }

        }

        private static void MakeMutants(ref Population population)
        {
            Random r = new Random();

            for (int i = 0; i < mutantCount; i++)
            {
                int mutantIndex;

                mutantIndex = r.Next(0, population.Mirrors.Count - 1);

                Mirror mutant = population.Mirrors[mutantIndex];

                Mutate(ref mutant);
            }
        }

        private static void KillWeakest(ref Population population, double focusPointPosition)
        {
            Comparison<Mirror> compare = CompareMirrors;
            population.Mirrors.Sort(compare);

            population.Mirrors.RemoveRange(population.Mirrors.Count - offspringCount - 1, offspringCount); //delete worst mirrors
        }

        /// <summary>
        /// Mutates a signle mirror. 
        /// </summary>
        /// <param name="mutant">Mirror to mutate.</param>
        private static void Mutate(ref Mirror mutant)
        {
            Random r = GlobalSettings.Instance.rnd;
            float max = 0, cur;
            int maxIndex = 0;

            for (int i = 0; i < mutant.GetSegmentCount(); i++)
            {
                cur = GeneticMath.SegmentFittnes(mutant, i);
                if (cur > max)
                {
                    maxIndex = i;
                    max = cur;
                }
            }

            int rand = r.Next(2);

            mutant.SetPoint(maxIndex + rand, mutant.GetPoint(maxIndex + rand) + GeneticMath.GetNormalDistributedRandom(0.0f, mutationStrength));

        }

        /// <summary>
        /// Makes a crossover of two mirrors.
        /// </summary>
        /// <param name="first">First mirror (daddy).</param>
        /// <param name="second">Second mirror (mommy).</param>
        /// <returns>Resulting mirror (child).</returns>
        private static Mirror Crossover(Mirror first, Mirror second)
        {
            if (first.GetPointCount() != second.GetPointCount())
                throw new ArgumentException("Mirrors must have equal point count");

            Mirror child = new Mirror();

            for (int index = 0; index < first.GetPointCount(); index++)
            {
                float a = (float)GlobalSettings.Instance.rnd.NextDouble();
                child.AddPoint((a*first.GetPoint(index) + (1-a)*second.GetPoint(index)));
            }

            return child;
        }

    }
}
