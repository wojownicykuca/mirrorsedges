﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MirrorsEdges
{
    class ResultMirror : IComparable
    {
        public ResultMirror(Mirror mirror, double fittnesFuncVal)
        {
            this.Mirror = mirror;
            this.FittnessFunctionValue = fittnesFuncVal;
        }

        public Mirror Mirror
        {
            get;
            set;
        }

        public double FittnessFunctionValue
        {
            get;
            set;
        }


        public int CompareTo(object obj)
        {
            ResultMirror secondMirror = (ResultMirror)obj;

            if (FittnessFunctionValue > secondMirror.FittnessFunctionValue) return 1;
            else if (FittnessFunctionValue < secondMirror.FittnessFunctionValue) return -1;
            else return 0;
        }
    }
}
