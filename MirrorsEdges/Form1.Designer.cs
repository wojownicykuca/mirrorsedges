﻿namespace MirrorsEdges
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.MirrorsNumber = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.FocusYBox = new System.Windows.Forms.NumericUpDown();
            this.FocusXBox = new System.Windows.Forms.NumericUpDown();
            this.GenerateBtn = new System.Windows.Forms.Button();
            this.PointsLabel = new System.Windows.Forms.Label();
            this.FocusYLabel = new System.Windows.Forms.Label();
            this.FocusXLabel = new System.Windows.Forms.Label();
            this.PointsNumberBox = new System.Windows.Forms.NumericUpDown();
            this.ReflectanceLabel = new System.Windows.Forms.Label();
            this.FocusPointLabel = new System.Windows.Forms.Label();
            this.SeveralNextMirrorBtn = new System.Windows.Forms.Button();
            this.PrevMirrorBtn = new System.Windows.Forms.Button();
            this.NextMirrorBtn = new System.Windows.Forms.Button();
            this.BestMirrosBtn = new System.Windows.Forms.Button();
            this.SeveralPrevMirrorsBtn = new System.Windows.Forms.Button();
            this.ConditionsGroupBox = new System.Windows.Forms.GroupBox();
            this.UpToGenerationConditionValueBox = new System.Windows.Forms.NumericUpDown();
            this.UpToReflectanceValueBox = new System.Windows.Forms.NumericUpDown();
            this.UpToGenerationConditionRadioBox = new System.Windows.Forms.RadioButton();
            this.UpToReflectanceRadioBox = new System.Windows.Forms.RadioButton();
            this.GenerationNumLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MirrorsNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsNumberBox)).BeginInit();
            this.ConditionsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpToGenerationConditionValueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpToReflectanceValueBox)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuPanel
            // 
            this.MenuPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MenuPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MenuPanel.Controls.Add(this.MirrorsNumber);
            this.MenuPanel.Controls.Add(this.label1);
            this.MenuPanel.Controls.Add(this.FocusYBox);
            this.MenuPanel.Controls.Add(this.FocusXBox);
            this.MenuPanel.Controls.Add(this.GenerateBtn);
            this.MenuPanel.Controls.Add(this.PointsLabel);
            this.MenuPanel.Controls.Add(this.FocusYLabel);
            this.MenuPanel.Controls.Add(this.FocusXLabel);
            this.MenuPanel.Controls.Add(this.PointsNumberBox);
            this.MenuPanel.Controls.Add(this.ReflectanceLabel);
            this.MenuPanel.Controls.Add(this.FocusPointLabel);
            this.MenuPanel.Controls.Add(this.SeveralNextMirrorBtn);
            this.MenuPanel.Controls.Add(this.PrevMirrorBtn);
            this.MenuPanel.Controls.Add(this.NextMirrorBtn);
            this.MenuPanel.Controls.Add(this.BestMirrosBtn);
            this.MenuPanel.Controls.Add(this.SeveralPrevMirrorsBtn);
            this.MenuPanel.Controls.Add(this.ConditionsGroupBox);
            this.MenuPanel.Controls.Add(this.GenerationNumLabel);
            this.MenuPanel.Location = new System.Drawing.Point(466, -1);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(184, 649);
            this.MenuPanel.TabIndex = 0;
            // 
            // MirrorsNumber
            // 
            this.MirrorsNumber.Location = new System.Drawing.Point(124, 390);
            this.MirrorsNumber.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.MirrorsNumber.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.MirrorsNumber.Name = "MirrorsNumber";
            this.MirrorsNumber.Size = new System.Drawing.Size(46, 20);
            this.MirrorsNumber.TabIndex = 17;
            this.MirrorsNumber.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.MirrorsNumber.ValueChanged += new System.EventHandler(this.MirrorsNumber_ValueChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 393);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "punktow w lustrze:";
            // 
            // FocusYBox
            // 
            this.FocusYBox.DecimalPlaces = 3;
            this.FocusYBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.FocusYBox.Location = new System.Drawing.Point(48, 140);
            this.FocusYBox.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.FocusYBox.Name = "FocusYBox";
            this.FocusYBox.Size = new System.Drawing.Size(79, 20);
            this.FocusYBox.TabIndex = 15;
            this.FocusYBox.ValueChanged += new System.EventHandler(this.FocusPointY_ValueChanged);
            // 
            // FocusXBox
            // 
            this.FocusXBox.DecimalPlaces = 3;
            this.FocusXBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.FocusXBox.Location = new System.Drawing.Point(48, 115);
            this.FocusXBox.Name = "FocusXBox";
            this.FocusXBox.Size = new System.Drawing.Size(79, 20);
            this.FocusXBox.TabIndex = 0;
            this.FocusXBox.ValueChanged += new System.EventHandler(this.FocusPointX_ValueChanged);
            // 
            // GenerateBtn
            // 
            this.GenerateBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GenerateBtn.Location = new System.Drawing.Point(10, 419);
            this.GenerateBtn.Name = "GenerateBtn";
            this.GenerateBtn.Size = new System.Drawing.Size(161, 28);
            this.GenerateBtn.TabIndex = 14;
            this.GenerateBtn.Text = "GENERUJ";
            this.GenerateBtn.UseVisualStyleBackColor = true;
            this.GenerateBtn.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // PointsLabel
            // 
            this.PointsLabel.AutoSize = true;
            this.PointsLabel.Location = new System.Drawing.Point(14, 366);
            this.PointsLabel.Name = "PointsLabel";
            this.PointsLabel.Size = new System.Drawing.Size(91, 13);
            this.PointsLabel.TabIndex = 13;
            this.PointsLabel.Text = "luster w populacji:";
            // 
            // FocusYLabel
            // 
            this.FocusYLabel.AutoSize = true;
            this.FocusYLabel.Location = new System.Drawing.Point(17, 142);
            this.FocusYLabel.Name = "FocusYLabel";
            this.FocusYLabel.Size = new System.Drawing.Size(17, 13);
            this.FocusYLabel.TabIndex = 12;
            this.FocusYLabel.Text = "Y:";
            // 
            // FocusXLabel
            // 
            this.FocusXLabel.AutoSize = true;
            this.FocusXLabel.Location = new System.Drawing.Point(17, 117);
            this.FocusXLabel.Name = "FocusXLabel";
            this.FocusXLabel.Size = new System.Drawing.Size(17, 13);
            this.FocusXLabel.TabIndex = 12;
            this.FocusXLabel.Text = "X:";
            // 
            // PointsNumberBox
            // 
            this.PointsNumberBox.Location = new System.Drawing.Point(124, 363);
            this.PointsNumberBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.PointsNumberBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.PointsNumberBox.Name = "PointsNumberBox";
            this.PointsNumberBox.Size = new System.Drawing.Size(46, 20);
            this.PointsNumberBox.TabIndex = 9;
            this.PointsNumberBox.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.PointsNumberBox.ValueChanged += new System.EventHandler(this.PointsNumber_ValueChanged);
            // 
            // ReflectanceLabel
            // 
            this.ReflectanceLabel.AutoSize = true;
            this.ReflectanceLabel.Location = new System.Drawing.Point(17, 48);
            this.ReflectanceLabel.Name = "ReflectanceLabel";
            this.ReflectanceLabel.Size = new System.Drawing.Size(135, 13);
            this.ReflectanceLabel.TabIndex = 8;
            this.ReflectanceLabel.Text = "Współczynnik zwierciadła:";
            // 
            // FocusPointLabel
            // 
            this.FocusPointLabel.AutoSize = true;
            this.FocusPointLabel.Location = new System.Drawing.Point(17, 92);
            this.FocusPointLabel.Name = "FocusPointLabel";
            this.FocusPointLabel.Size = new System.Drawing.Size(83, 13);
            this.FocusPointLabel.TabIndex = 7;
            this.FocusPointLabel.Text = "Punkt skupienia";
            // 
            // SeveralNextMirrorBtn
            // 
            this.SeveralNextMirrorBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SeveralNextMirrorBtn.Location = new System.Drawing.Point(134, 175);
            this.SeveralNextMirrorBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SeveralNextMirrorBtn.Name = "SeveralNextMirrorBtn";
            this.SeveralNextMirrorBtn.Size = new System.Drawing.Size(37, 23);
            this.SeveralNextMirrorBtn.TabIndex = 6;
            this.SeveralNextMirrorBtn.Text = ">>";
            this.SeveralNextMirrorBtn.UseVisualStyleBackColor = true;
            this.SeveralNextMirrorBtn.Click += new System.EventHandler(this.NextSeveral_Click);
            // 
            // PrevMirrorBtn
            // 
            this.PrevMirrorBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PrevMirrorBtn.Location = new System.Drawing.Point(51, 175);
            this.PrevMirrorBtn.Margin = new System.Windows.Forms.Padding(2);
            this.PrevMirrorBtn.Name = "PrevMirrorBtn";
            this.PrevMirrorBtn.Size = new System.Drawing.Size(37, 23);
            this.PrevMirrorBtn.TabIndex = 5;
            this.PrevMirrorBtn.Text = "<";
            this.PrevMirrorBtn.UseVisualStyleBackColor = true;
            this.PrevMirrorBtn.Click += new System.EventHandler(this.Prev_Click);
            // 
            // NextMirrorBtn
            // 
            this.NextMirrorBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NextMirrorBtn.Location = new System.Drawing.Point(93, 175);
            this.NextMirrorBtn.Margin = new System.Windows.Forms.Padding(2);
            this.NextMirrorBtn.Name = "NextMirrorBtn";
            this.NextMirrorBtn.Size = new System.Drawing.Size(37, 23);
            this.NextMirrorBtn.TabIndex = 4;
            this.NextMirrorBtn.Text = ">";
            this.NextMirrorBtn.UseVisualStyleBackColor = true;
            this.NextMirrorBtn.Click += new System.EventHandler(this.Next_Click);
            // 
            // BestMirrosBtn
            // 
            this.BestMirrosBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BestMirrosBtn.Location = new System.Drawing.Point(10, 203);
            this.BestMirrosBtn.Margin = new System.Windows.Forms.Padding(2);
            this.BestMirrosBtn.Name = "BestMirrosBtn";
            this.BestMirrosBtn.Size = new System.Drawing.Size(161, 23);
            this.BestMirrosBtn.TabIndex = 3;
            this.BestMirrosBtn.Text = "Najlepsze zwierciadło";
            this.BestMirrosBtn.UseVisualStyleBackColor = true;
            this.BestMirrosBtn.Click += new System.EventHandler(this.Best_Click);
            // 
            // SeveralPrevMirrorsBtn
            // 
            this.SeveralPrevMirrorsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SeveralPrevMirrorsBtn.Location = new System.Drawing.Point(10, 175);
            this.SeveralPrevMirrorsBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SeveralPrevMirrorsBtn.Name = "SeveralPrevMirrorsBtn";
            this.SeveralPrevMirrorsBtn.Size = new System.Drawing.Size(37, 23);
            this.SeveralPrevMirrorsBtn.TabIndex = 2;
            this.SeveralPrevMirrorsBtn.Text = "<<";
            this.SeveralPrevMirrorsBtn.UseVisualStyleBackColor = true;
            this.SeveralPrevMirrorsBtn.Click += new System.EventHandler(this.PrevSeveral_Click);
            // 
            // ConditionsGroupBox
            // 
            this.ConditionsGroupBox.Controls.Add(this.UpToGenerationConditionValueBox);
            this.ConditionsGroupBox.Controls.Add(this.UpToReflectanceValueBox);
            this.ConditionsGroupBox.Controls.Add(this.UpToGenerationConditionRadioBox);
            this.ConditionsGroupBox.Controls.Add(this.UpToReflectanceRadioBox);
            this.ConditionsGroupBox.Location = new System.Drawing.Point(10, 243);
            this.ConditionsGroupBox.Name = "ConditionsGroupBox";
            this.ConditionsGroupBox.Size = new System.Drawing.Size(161, 106);
            this.ConditionsGroupBox.TabIndex = 1;
            this.ConditionsGroupBox.TabStop = false;
            this.ConditionsGroupBox.Text = "Warunki zakończenia";
            // 
            // UpToGenerationConditionValueBox
            // 
            this.UpToGenerationConditionValueBox.Enabled = false;
            this.UpToGenerationConditionValueBox.Location = new System.Drawing.Point(42, 80);
            this.UpToGenerationConditionValueBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.UpToGenerationConditionValueBox.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.UpToGenerationConditionValueBox.Name = "UpToGenerationConditionValueBox";
            this.UpToGenerationConditionValueBox.Size = new System.Drawing.Size(94, 20);
            this.UpToGenerationConditionValueBox.TabIndex = 3;
            this.UpToGenerationConditionValueBox.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.UpToGenerationConditionValueBox.ValueChanged += new System.EventHandler(this.UpToGeneration_ValueChanged);
            // 
            // UpToReflectanceValueBox
            // 
            this.UpToReflectanceValueBox.DecimalPlaces = 3;
            this.UpToReflectanceValueBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.UpToReflectanceValueBox.Location = new System.Drawing.Point(42, 39);
            this.UpToReflectanceValueBox.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.UpToReflectanceValueBox.Name = "UpToReflectanceValueBox";
            this.UpToReflectanceValueBox.Size = new System.Drawing.Size(94, 20);
            this.UpToReflectanceValueBox.TabIndex = 2;
            this.UpToReflectanceValueBox.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.UpToReflectanceValueBox.ValueChanged += new System.EventHandler(this.UpToReflectance_ValueChanged);
            // 
            // UpToGenerationConditionRadioBox
            // 
            this.UpToGenerationConditionRadioBox.AutoSize = true;
            this.UpToGenerationConditionRadioBox.Location = new System.Drawing.Point(6, 61);
            this.UpToGenerationConditionRadioBox.Name = "UpToGenerationConditionRadioBox";
            this.UpToGenerationConditionRadioBox.Size = new System.Drawing.Size(132, 17);
            this.UpToGenerationConditionRadioBox.TabIndex = 1;
            this.UpToGenerationConditionRadioBox.Text = "do narodzin pokolenia:";
            this.UpToGenerationConditionRadioBox.UseVisualStyleBackColor = true;
            this.UpToGenerationConditionRadioBox.CheckedChanged += new System.EventHandler(this.UpToGenerationConditionRadioBox_CheckedChanged);
            // 
            // UpToReflectanceRadioBox
            // 
            this.UpToReflectanceRadioBox.AutoSize = true;
            this.UpToReflectanceRadioBox.Checked = true;
            this.UpToReflectanceRadioBox.Location = new System.Drawing.Point(6, 19);
            this.UpToReflectanceRadioBox.Name = "UpToReflectanceRadioBox";
            this.UpToReflectanceRadioBox.Size = new System.Drawing.Size(114, 17);
            this.UpToReflectanceRadioBox.TabIndex = 0;
            this.UpToReflectanceRadioBox.TabStop = true;
            this.UpToReflectanceRadioBox.Text = "do współczynnika:";
            this.UpToReflectanceRadioBox.UseVisualStyleBackColor = true;
            this.UpToReflectanceRadioBox.CheckedChanged += new System.EventHandler(this.UpToReflectance_CheckedChanged);
            // 
            // GenerationNumLabel
            // 
            this.GenerationNumLabel.AutoSize = true;
            this.GenerationNumLabel.Location = new System.Drawing.Point(17, 22);
            this.GenerationNumLabel.Name = "GenerationNumLabel";
            this.GenerationNumLabel.Size = new System.Drawing.Size(90, 13);
            this.GenerationNumLabel.TabIndex = 0;
            this.GenerationNumLabel.Text = "Numer pokolenia:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(466, 647);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 647);
            this.Controls.Add(this.MenuPanel);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Zwierciadła";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MenuPanel.ResumeLayout(false);
            this.MenuPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MirrorsNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsNumberBox)).EndInit();
            this.ConditionsGroupBox.ResumeLayout(false);
            this.ConditionsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpToGenerationConditionValueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpToReflectanceValueBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox ConditionsGroupBox;
        private System.Windows.Forms.RadioButton UpToGenerationConditionRadioBox;
        private System.Windows.Forms.RadioButton UpToReflectanceRadioBox;
        private System.Windows.Forms.Label GenerationNumLabel;
        private System.Windows.Forms.Button SeveralNextMirrorBtn;
        private System.Windows.Forms.Button PrevMirrorBtn;
        private System.Windows.Forms.Button NextMirrorBtn;
        private System.Windows.Forms.Button BestMirrosBtn;
        private System.Windows.Forms.Button SeveralPrevMirrorsBtn;
        private System.Windows.Forms.Label ReflectanceLabel;
        private System.Windows.Forms.Label FocusPointLabel;
        private System.Windows.Forms.NumericUpDown PointsNumberBox;
        private System.Windows.Forms.Button GenerateBtn;
        private System.Windows.Forms.Label PointsLabel;
        private System.Windows.Forms.Label FocusYLabel;
        private System.Windows.Forms.Label FocusXLabel;
        private System.Windows.Forms.NumericUpDown FocusXBox;
        private System.Windows.Forms.NumericUpDown FocusYBox;
        private System.Windows.Forms.NumericUpDown UpToGenerationConditionValueBox;
        private System.Windows.Forms.NumericUpDown UpToReflectanceValueBox;
        private System.Windows.Forms.NumericUpDown MirrorsNumber;
        private System.Windows.Forms.Label label1;

    }
}

